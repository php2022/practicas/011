<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        /**
         * Esta funcion genera una serie de numeros aleatorios
         * @param int $minimo Este es el valor minimo
         * @param int $maximo Este es el valor maximo
         * @param int $numero Numero de valores a generar
         * @param int[] $salida Es el array donde se almacenara la salida
         */
        function ejercicio2($minimo,$maximo,$numero,&$salida){
            /*
             * Inicializando el array para evitar que se quede con valores anteriores
             */
            $salida=[];
            
            /*
             * Bucle para rellenar el array
             */
            for($c=0;$c<$numero;$c++){
                $salida[$c]= mt_rand($minimo,$maximo);
            }
        }
        ?>
    </body>
</html>
