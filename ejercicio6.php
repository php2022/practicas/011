<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        /**
         * Funcion que te permite generar un array con el contenido de un directorio.La salida esta ordenada
         * @param type $handle cadena de caracteres con la ruta del directorio a listar
         * @return string[] array con el nombre de los ficheros y directorios
         */
        function leerDirectorio($handle="."){
            $handle= opendir($handle);
            while(false!==($archivo=readdir($handle))){
                $archivos[]=strtolower($archivo);
            }
            closedir($handle);
            sort($archivos);
            return $archivos;
        }
        
        var_dump(leerDirectorio());
        ?>
    </body>
</html>
